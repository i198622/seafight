'use strict';

var SeaFight = function (options) {
    
    // Default settings
    var defaults = {
        colls: 10,
        rows: 10,
        ships: [{4:1}, {3:2}, {2:3}, {1:4}],
        markerColls: ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К'],
        markerRows: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        fieldSelfSelector: '#battle_field_self',
        fieldRivalSelector: '#battle_field_rival',
        firebasePath: 'https://seafight.firebaseio.com/'
    };
    options = options || {};

    this.settings = _.extend(defaults, options);
    this.map = [];
    this.selfShips = [];
    this.rivalShips = [];

    this.selfWholeShips = 0;
    this.rivalWholeShips = 0;
    
    this.isGameStarted = false;
    this.gameMode = 0;  // 0:Computer 1:Friend
    this.whooseTurn = 0 // 0:self 1:rival
    this.haveNotFinishedShips = [];
    /**
     * Generate base map matrix
     * @returns null
     */
    this.generateMap = function() {
        for(var r=0; r<this.settings.rows; r++) {
            this.map.push([]);
            for(var c=0; c<this.settings.colls; c++) {
                var data = {
                    x:c,
                    y:r,
                    empty:true,
                    missed: false,
                    ship: false,
                    padded: false
                };

                this.map[r].push(data);
            }
        }

        // Deep clone map
        this.selfShips = JSON.parse(JSON.stringify(this.map));
        this.rivalShips = JSON.parse(JSON.stringify(this.map));
    }


    /**
     * Generate and draw html field
     * @returns null
     */
    this.generateFiled = function () {

        var $fieldSelf = document.querySelector(this.settings.fieldSelfSelector);
        var $fieldRival = document.querySelector(this.settings.fieldRivalSelector);

        $fieldSelf.classList.add('active');

        // Create overlay
        var overlayHtml = '<div class="battle__controls active"><h3>Противник</h3>';
        overlayHtml += '<div class="battle__rival-variants">';
        overlayHtml += '<a class="battle__rival-variant active" data-rivar-variant="0">Случайный</a>';
        overlayHtml += '<a class="battle__rival-variant" data-rivar-variant="1">Знакомый</a></div>';
        overlayHtml += '<div class="battle__play-with-friend">';
        overlayHtml += '<label class="battle__link-label">Скопируйте и отправьте ссылку</label><input type="text" value="" id="battle_link" class="battle__link-input" /> </div>'
        overlayHtml += '<a class="battle__play-btn battle_start_play">Играть</a> </div>';
        $fieldRival.insertAdjacentHTML('beforeend', overlayHtml);

        // Create disabled layer
        var disableLayer = '<div class="battle_disabled"></div>';
        $fieldRival.insertAdjacentHTML('beforeend', disableLayer);

        // Generate markers
        var markerRowsHtml = '<ul class="battle__marker battle__marker-row">';
        var markerCollsHtml = '<ul class="battle__marker battle__marker-coll">';

        _.each(this.settings.markerRows, function(row) {
            markerRowsHtml += "<li>" + row + "</li>";
        });
        markerRowsHtml += "</ul>";

        $fieldSelf.insertAdjacentHTML('beforeend', markerRowsHtml);
        $fieldRival.insertAdjacentHTML('beforeend', markerRowsHtml);

        _.each(this.settings.markerColls, function(coll) {
            markerCollsHtml += "<li>" + coll + "</li>";
        });
        markerCollsHtml += "</ul>";

        $fieldSelf.insertAdjacentHTML('beforeend', markerCollsHtml);
        $fieldRival.insertAdjacentHTML('beforeend', markerCollsHtml);

        this._drawFieldGrid(this.selfShips, $fieldSelf);
        this._drawFieldGrid(this.map, $fieldRival);
    }

    this._drawFieldGrid  = function(userMap, elm) {
        _.each(userMap, function(row, rowIndex) {
            _.each(row, function(coll, collIndex) {
                var cellElm = document.createElement('div');
                cellElm.classList.add('battle__field-cell');
                cellElm.setAttribute("data-y", rowIndex);
                cellElm.setAttribute("data-x", collIndex);

                if(coll.ship === true) {
                    cellElm.classList.add('ship');
                }

                if(coll.shipDiv == true) {
                    var shipElm = document.createElement('div');
                    
                    shipElm.classList.add('battle__ship');
                    
                    if(coll.position == 'h') {
                        shipElm.style.width = 1.98 * coll.length  + 'em';
                    } else {
                        shipElm.style.height = 1.98 * coll.length  + 'em';
                    }

                    cellElm.appendChild(shipElm);
                }

                elm.appendChild(cellElm);
            }, rowIndex);
        });
    }


    this._markFieldShip = function(row, coll, map) {

        if(coll - 1 !== -1) {
            _.extend(map[row][coll - 1], {empty: false});
        }

        if((coll + 1 >= this.settings.colls) == false) {
            _.extend(map[row][coll + 1], {empty: false});
        }

        if(row - 1 !== -1) {
            _.extend(map[row - 1][coll], {empty: false});
            _.extend(map[row - 1][coll - 1], {empty: false});
            _.extend(map[row - 1][coll + 1], {empty: false});
        }

        if((row +1 >= this.settings.rows) == false) {
            _.extend(map[row + 1][coll], {empty: false});
            _.extend(map[row + 1][coll - 1], {empty: false});
            _.extend(map[row + 1][coll + 1], {empty: false});
        }

    }

    /**
     * Generate and draw html field
     * @returns {Object}
     */
    this._setShipPosition = function(shipLength, userMap) {

        var data = {};
        data.position = _.random(0, 1) ? 'h' : 'v';

        if(data.position == 'v') {
            data.row = _.random(0, this.settings.rows - shipLength -1);
            data.coll = _.random(0, this.settings.colls - 1);

        } else {
            data.row = _.random(0, this.settings.rows - 1);
            data.coll = _.random(0, this.settings.colls - shipLength - 1);
        }

        for(var i=0; i<shipLength; i++) {
            if(data.position == 'h') {
                if(userMap[data.row][data.coll + i].empty == false) {
                    return this._setShipPosition(shipLength, userMap);
                }
            } else {
                if(userMap[data.row + i][data.coll].empty == false) {
                    return this._setShipPosition(shipLength, userMap);
                }
            }
        }

        // change ship settings
        for(var i=0; i<shipLength; i++) {
            var shipOptions = {
                length: shipLength,
                ship: true,
                empty: false,
                position: data.position
            };
            if(data.position === 'h') {
                _.extend(userMap[data.row][data.coll + i], shipOptions);
                this._markFieldShip(data.row, data.coll + i, userMap);
            } else {
                _.extend(userMap[data.row + i][data.coll], shipOptions);
                this._markFieldShip(data.row + i, data.coll, userMap);
            }
        }

        // set ship flag
        _.extend(userMap[data.row][data.coll], {shipDiv: true});
        return data;
    }


    /**
     * Generate ships
     * @returns null
     */
    this.generateShips = function() {
        _.each(this.settings.ships, function(ship) {
            var shipLength = parseInt(_.keys(ship)[0]);
            var shipsCount = parseInt(_.values(ship)[0]);

            this.selfWholeShips += shipLength * shipsCount;
            this.rivalWholeShips += shipLength * shipsCount;

            for(var i=0; i<shipsCount; i++) {
                this._setShipPosition(shipLength, this.selfShips);
                this._setShipPosition(shipLength, this.rivalShips);
            }
        }, this);
    }


    this._loadScript = function (url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        script.onreadystatechange = callback;
        script.onload = callback;

        head.appendChild(script);
    }


    this.stopGame = function() {

        if(this.rivalWholeShips == 0) {
            alert('Поздравляем вы победитель.');
        } else {
            alert('Не переживай!');
        }

        var $fieldSelf = document.querySelector(this.settings.fieldSelfSelector);
        var $fieldRival = document.querySelector(this.settings.fieldRivalSelector);

        while ($fieldSelf.firstChild) {
            $fieldSelf.removeChild($fieldSelf.firstChild);
        }

        while ($fieldRival.firstChild) {
            $fieldRival.removeChild($fieldRival.firstChild);
        }

        // reset to default settings
        this.map = [];
        this.selfShips = [];
        this.rivalShips = [];

        this.selfWholeShips = 0;
        this.rivalWholeShips = 0;
        
        this.isGameStarted = false;
        this.gameMode = 0;  // 0:Computer 1:Friend
        this.whooseTurn = 0 // 0:self 1:rival
        this.haveNotFinishedShips = [];

        this.init();
    }


    this._toggleField = function() {
        var $fieldSelf = document.querySelector(this.settings.fieldSelfSelector);
        var $fieldRival = document.querySelector(this.settings.fieldRivalSelector);

        if(this.whooseTurn == 0) {
            $fieldSelf.classList.add('active');
            $fieldRival.classList.remove('active');
            this.whooseTurn = 1;
        } else {
            $fieldSelf.classList.remove('active');
            $fieldRival.classList.add('active');
            this.whooseTurn = 0;
        }
    }


    this._markAsMissedPoint = function(map, row, coll) {

        if(this.rivalWholeShips == 0 || this.selfWholeShips == 0) {
            return this.stopGame();
        }

        map[row][coll].missed = true;

        var $field = document.querySelector(this.settings.fieldRivalSelector);

        if(this.whooseTurn == 1) {
            $field = document.querySelector(this.settings.fieldSelfSelector);
        }

        var $cell = $field.querySelector('[data-x="' + coll + '"][data-y="' + row + '"]');
        if($cell) {
            $cell.classList.add('missed');
        }
        
    }


    this._markAsKilledShip = function(map, shipDivId) {

        var row = shipDivId.y;
        var coll = shipDivId.x;
        var cell = map[row][coll];

        var $field = document.querySelector(this.settings.fieldRivalSelector);

        if(this.whooseTurn == 1) {
            $field = document.querySelector(this.settings.fieldSelfSelector);
        }

        var ship = $field.querySelector('[data-x="' + coll + '"][data-y="' + row + '"] .battle__ship');
        
        if(_.isNull(ship)) {
            var shipElm = document.createElement('div');
            
            shipElm.classList.add('battle__ship');
            shipElm.classList.add('killed');
            
            if(cell.position == 'h') {
                shipElm.style.width = 1.98 * cell.length  + 'em';
            } else {
                shipElm.style.height = 1.98 * cell.length  + 'em';
            }

            $field.querySelector('[data-x="' + coll + '"][data-y="' + row + '"]').appendChild(shipElm);
            
        } else {
            ship.classList.add('killed');
        }


        if(cell.position == 'h' || cell.length == 1) {
            if(coll -1 !== -1) {
                map[row][coll - 1].missed = true;
                $field.querySelector('[data-x="' + (coll - 1) + '"][data-y="' + (row) + '"]').classList.add('missed');
            }

            if((coll + cell.length) <= (this.settings.rows - 1)) {
                map[row][coll + cell.length].missed = true;
                $field.querySelector('[data-x="' + (coll + cell.length) + '"][data-y="' + (row) + '"]').classList.add('missed');
            }

        } 

        if(cell.position == 'v' || cell.length == 1) {
            if(row -1 !== -1) {
                map[row - 1][coll].missed = true;
                $field.querySelector('[data-x="' + (coll) + '"][data-y="' + (row - 1) + '"]').classList.add('missed');
            }

            if((row + cell.length) <= (this.settings.rows - 1)) {
                map[row + cell.length][coll].missed = true;
                $field.querySelector('[data-x="' + (coll) + '"][data-y="' + (row + cell.length) + '"]').classList.add('missed');
            }
        }
    }


    this._markAsPaddedPoint = function(map, row, coll) {
        
        map[row][coll].padded = true;

        var point = map[row][coll];
        var $field = document.querySelector(this.settings.fieldRivalSelector);

        if(this.whooseTurn == 1) {
            $field = document.querySelector(this.settings.fieldSelfSelector);
        }

        var $cell = $field.querySelector('[data-x="' + coll + '"][data-y="' + row + '"]');
        $cell.classList.add('wounded');

        if(row - 1 !== -1 && coll - 1 !== -1) {
            map[row - 1][coll - 1].missed = true;
            $field.querySelector('[data-x="' + (coll - 1) + '"][data-y="' + (row - 1) + '"]').classList.add('missed');
        }

        if(row - 1 !== -1 && (coll +1 >= this.settings.rows) == false) {
            map[row - 1][coll + 1].missed = true;
            $field.querySelector('[data-x="' + (coll + 1) + '"][data-y="' + (row - 1) + '"]').classList.add('missed');
        }

        if((row + 1 >= this.settings.rows) == false && coll - 1 !== -1) {
            map[row + 1][coll - 1].missed = true;
            $field.querySelector('[data-x="' + (coll - 1) + '"][data-y="' + (row + 1) + '"]').classList.add('missed');
        }

        if((row + 1 >= this.settings.rows) == false && (coll + 1 >= this.settings.colls) == false) {
            map[row + 1][coll + 1].missed = true;
            $field.querySelector('[data-x="' + (coll + 1) + '"][data-y="' + (row + 1) + '"]').classList.add('missed');
        }

        var counter = 0;
        var shipDivId = 0;
        var cell;

        if(point.position == 'h') {
            for(var i=0; i<map[row].length; i++) {
                cell = map[row][i];

                if(cell.shipDiv) {
                    shipDivId = cell;
                }

                if(cell.ship == true && cell.padded == true) {
                    counter += 1;
                } else {
                    counter = 0;
                }

                if(cell.padded == true && cell.length == counter ) {
                    this._markAsKilledShip(map, shipDivId);
                }
            }
        } else {
            for(var i=0; i<map[row].length; i++) {
                cell = map[i][coll];

                if(cell.shipDiv) {
                    shipDivId = cell;
                }

                if(cell.ship == true && cell.padded == true) {
                    counter += 1;
                } else {
                    counter = 0;
                }

                if(cell.padded == true && cell.length == counter ) {
                    this._markAsKilledShip(map, shipDivId);
                }
            }
        }

        if(this.rivalWholeShips == 0 || this.selfWholeShips == 0) {
            return this.stopGame();
        }
    }



    this._humanTurn = function() {
        
        var _this = this;
        var $fieldRival = document.querySelector(this.settings.fieldRivalSelector);
        var $fieldSelf = document.querySelector(this.settings.fieldSelfSelector);

        $fieldRival.addEventListener('click', function(e) {
            var $elm = e.target;
            
            if($elm.classList.contains('battle__field-cell')) {
                var coll = parseInt($elm.getAttribute('data-x'));
                var row = parseInt($elm.getAttribute('data-y'));
                var point = _this.rivalShips[row][coll];
                
                if(point.missed === false) {

                    if(point.ship === false) {
                        // Если пустое место
                        _this._markAsMissedPoint(_this.rivalShips, row, coll);
                        _this._toggleField();
                        _this._compTurn();
                    } else if(point.ship === true && point.padded === false) {
                        // Если корабль не подбит
                        _this.rivalWholeShips -= 1;
                        _this._markAsPaddedPoint(_this.rivalShips, row, coll);
                    }
                }
            }
        }, false);

    }


    this._compTurn = function(hit) {
        
        var _this = this;
        var progressDelay = _.random(50, 400);
        
        var remainingCells = _.where(_.flatten(this.selfShips), {missed: false});
        var r = _.random(0, remainingCells.length - 1);
        var point;
        var row;
        var coll;
        var finishOf = [];
        
        if(_.isUndefined(hit) && this.haveNotFinishedShips.length == 0) {
            point = remainingCells[r];
            coll = point.x;
            row = point.y;
        } else if(this.haveNotFinishedShips.length !== 0) {
        
            finishOf = this.haveNotFinishedShips;

        } else {
            if(hit.y - 1 !== -1) {
                finishOf.push({y: hit.y - 1, x:hit.x});
            }

            if(hit.y + 1 <= this.settings.rows - 1) {
                finishOf.push({y: hit.y + 1, x:hit.x});
            }

            if(hit.x - 1 !== -1) {
                finishOf.push({y: hit.y, x:hit.x - 1});
            }

            if(hit.x + 1 <= this.settings.colls - 1) {
                finishOf.push({y: hit.y, x:hit.x + 1});
            }
        }

        if(finishOf.length !== 0) {
            r = _.random(0, finishOf.length - 1);
            var randFinishOf = finishOf[r];
            point = this.selfShips[randFinishOf.y][randFinishOf.x];
            row = randFinishOf.y;
            coll = randFinishOf.x;
            finishOf = _.without(finishOf, randFinishOf);
            this.haveNotFinishedShips = finishOf;
        }

        if(point.missed == false) {
            if(point.ship === false) {
                // Если пустое место
                _this._markAsMissedPoint(_this.selfShips, row, coll);
                _this._toggleField();
                _this._humanTurn();
                
            } else if(point.ship === true && point.padded === false) {
                // Если корабль не подбит
                _this.selfWholeShips -= 1;;
                setTimeout(function() {
                    _this._markAsPaddedPoint(_this.selfShips, row, coll);
                    _this._compTurn(point);
                }, progressDelay);
            } else {
                setTimeout(function() {
                    _this._compTurn();
                }, progressDelay);
            }
        } else {
            setTimeout(function() {
                _this._compTurn();
            }, progressDelay);
        }

    }


    this.playWithComp = function() {
        var $fieldSelf = document.querySelector(this.settings.fieldSelfSelector);
        var $fieldRival = document.querySelector(this.settings.fieldRivalSelector);

        // hide controlls
        var $contols = $fieldRival.querySelector('.battle__controls');
        $contols.classList.remove('active');

        this.whooseTurn = _.random(0,1);
        this._toggleField();

        if(this.whooseTurn == 0) {
            this._humanTurn();
        } else {
            this._compTurn();
        }

    }


    this.playWithFriend = function() {
        var _this = this;

        if(typeof Firebase == 'undefined') {
            this._loadScript('https://cdn.firebase.com/js/client/2.0.6/firebase.js', function() {
                
                var hash = window.location.hash.replace('#', '');
                console.log(window.location.hash);

                var mainRef = new Firebase(_this.settings.firebasePath);
                var gameRef = mainRef.child('games/' + hash);
                
                gameRef.set({
                    rivalShips: _this.rivalShips,
                    selfShips: _this.selfShips,
                    map: _this.map,
                    whooseTurn: _this.whooseTurn
                });

                gameRef.onDisconnect().remove();

            });
        }
    }


    this.init = function() {
        this.generateMap();
        this.generateShips();
        this.generateFiled();

        var $variants = document.querySelectorAll(".battle__rival-variant");
        var $advInfo = document.querySelector('.battle__play-with-friend');
        var $playBtn = document.querySelector('.battle_start_play');
        var $overlay = document.querySelector('.battle__controls');
        var _this = this;

        _.each($variants, function(elm, index) {
            elm.addEventListener('click', function(e) {
                _.each($variants, function(cl) {
                    cl.classList.remove("active");
                });
                
                _this.gameMode = e.target.getAttribute("data-rivar-variant");
                
                if(_this.gameMode == 1) {
                    $advInfo.classList.add("active");
                    
                    if(window.location.hash == false) {
                        var n=Math.floor(Math.random()*11);
                        var k = Math.floor(Math.random()* 1000000);
                        var m = String.fromCharCode(n)+k;

                        var uniqUrl = window.location.href + '#id-' + m;
                        document.querySelector('#battle_link').setAttribute('value', uniqUrl);
                    }

                } else {
                    $advInfo.classList.remove("active");
                }

                e.target.classList.add("active");

            },false);
        });


        // Start play with comp or friend
        $playBtn.addEventListener('click', function(e) {
            if(_this.gameMode == 0) {
                _this.playWithComp();
            } else {
                _this.playWithFriend();
            }
            $overlay.classList.remove('active');
        }, false);
    }

    this.init();
};








